import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    redirect: "/quiz/1",
  },
  {
    path: "/quiz/:id",
    name: "Quiz",
    component: () => import("@/views/Quiz"),
  },
  {
    path: "/result",
    name: "Result",
    component: () => import("@/views/Result"),
  },
  {
    path: "/quiz-list",
    name: "QuizList",
    component: () => import("@/views/QuizList"),
  },
  {
    path: "/quiz-detail/:id",
    name: "Result",
    component: () => import("@/views/SingleQuiz"),
  },
  {
    path: "/*",
    name: "404",
    component: () => import("@/views/404"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

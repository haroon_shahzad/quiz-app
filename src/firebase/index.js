import firebase from "firebase/app";
import "firebase/firestore";

var firebaseConfig = {
  apiKey: "AIzaSyD-Xrl3mEG7glY794N3FiQPK8QY1iYkx24",
  authDomain: "quiz-app-9d8c4.firebaseapp.com",
  projectId: "quiz-app-9d8c4",
  storageBucket: "quiz-app-9d8c4.appspot.com",
  messagingSenderId: "336767014931",
  appId: "1:336767014931:web:7bd47e3b4dedd546a287e6",
  measurementId: "G-F45Y1KYYSC",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export default db;

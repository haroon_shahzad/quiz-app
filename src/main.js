import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "animate.css";
import "./scss/main.scss";

Vue.config.productionTip = false;

import Loader from "@/components/Loader";
Vue.component("loader", Loader);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
